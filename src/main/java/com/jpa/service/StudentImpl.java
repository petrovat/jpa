package com.jpa.service;

import java.util.List;
import java.util.Optional;

import com.jpa.model.Student;
import com.jpa.view.StudentRepo;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentImpl implements StudentRepo {

   
@Autowired
public SessionFactory sessionFactory;


public void addStudent(Student student) {
    sessionFactory.getCurrentSession().saveOrUpdate(student);

}

    @Override
    public <S extends Student> S save(S entity) {
        return null;
    }

    @Override
    public <S extends Student> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Student> findById(Long id) {
        return null;
    }

    @Override
    public boolean existsById(Long id) {
        return false;
    }

    @Override
    public Iterable<Student> findAll() {
        return null;
    }

    @Override
    public Iterable<Student> findAllById(Iterable<Long> ids) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void delete(Student entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends Student> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Student> getAllStudents() {
        return sessionFactory.getCurrentSession().createQuery("from Student")
                .list();
    }

    @Override
    public void deleteStudent(Integer studentId) {
        Student student = (Student) sessionFactory.getCurrentSession().load(
            Student.class, studentId);
    if (null != student) {
        this.sessionFactory.getCurrentSession().delete(student);
    }
    }

    @Override
    public Student updateStudent(Student student) {
        sessionFactory.getCurrentSession().update(student);
        return student;    }

    @Override
    public Student getStudent(int studentid) {
        return (Student) sessionFactory.getCurrentSession().get(
            Student.class, studentid);    }


    



















/* 
    @Autowired
    private StudentRepo studentrepository; */

  

      /* public void addStudent(Student student) {
        //studentrepository.save(student);
        student.add(student);
    } */




}