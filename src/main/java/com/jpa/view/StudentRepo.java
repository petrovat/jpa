package com.jpa.view;

import java.util.List;

import com.jpa.model.Student;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;


@EnableJpaRepositories
public interface StudentRepo extends CrudRepository<Student, Long> {
/* 
	Student save(List<Student> students);

	Optional<Student> findById(Long id);

    List<Student> findByLastName(@Param ("q") String lastName);

	List<Student> findAll(); */

	public void addStudent(Student student);
 
    public List<Student> getAllStudents();
 
    public void deleteStudent(Integer studentId);
 
    public Student updateStudent(Student student);
 
    public Student getStudent(int studentid);
}

/* 	public default void addStudent(Student student) {
	} */




	 

  //public List<Student> saveStudent(Student student);

	//List<Student> getAllStudentsList();

	//Student saveStudent(List<Student> students);

 

