package com.jpa.model;

import javax.persistence.*;




@Entity
    @Table(name = "student")
    public class Student {
      
        @Id
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long id;
    
      
        @Column(name="first_name")
        private String firstName;
    
        @Column(name= "last_name")
        private String lastName;
    
        @Column(name="age")
        private int age;

        
    
        public Student(Long id, String firstName, String lastName, int age) {
		}

		public Student() {
		}

		public void setFirstName( String firstName){
            this.firstName= firstName;
        }
    
        public String getFirstName(){
            return firstName;
        }
    
    
        public void setAge( int age){
            this.age= age;
        }
    
        public Integer getAge(){
            return age;
        }
    
    
        public void setId( long i){
            this.id= i;
        }
    
        public Long getId(){
            return id;
        }
    
    
        public void setLastName( String lastName){
            this.lastName= lastName;
        }
    
        public String getLastName(){
            return lastName;
        }

		public void add(Student student) {
		}
    
    
    
    }
    
    
    