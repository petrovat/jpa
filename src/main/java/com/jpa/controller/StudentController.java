package com.jpa.controller;

import java.util.List;
//import javax.persistence.*;

import com.jpa.model.Student;
import com.jpa.service.StudentImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@CrossOrigin
@RequestMapping(value = "/schoolSlist")

public class StudentController {
  
   @Autowired
    private StudentImpl studentimpl; 

  /* public Optional<Student> readItem(@PathVariable Long fruitId) {
        Optional<Student> student = this.studentRepo.findById(fruitId);
    return student;
   }  */


   
 
    @GetMapping
    public List<Student> getAllStudentList()
    {
      
        return (List<Student>) studentimpl.findAll();
    }
  

    @RequestMapping(value = "/saveEmployee", method = RequestMethod.POST)
    public ModelAndView saveEmployee(@ModelAttribute Student student) {
        if (student.getId() == 0) { // if student id is 0 then creating the
            // student other updating the student
            studentimpl.addStudent(student);
        } else {
            studentimpl.updateStudent(student);
        }
        return new ModelAndView("redirect:/");
    }
   
    
    @GetMapping(path="add/student")
    Student addStudent(Student student){

    student.setAge(22);
    student.setFirstName("karabo");
    student.setId(1);
    student.setLastName("Mogorosi");

        return (Student) studentimpl.save(student);
    }

    @GetMapping(path="studentDisplay")
    Student addStudent(List<Student> students) {

        Student student_One =new Student();
        student_One.setAge(21);
        student_One.setFirstName("toddy");
        student_One.setLastName("petrova");
        students.add(student_One);

        Student student_Two =new Student();
        student_One.setAge(17);
        student_One.setFirstName("Mabedi");
        student_One.setLastName("Motimedi");
        students.add(student_Two);
        
        return (Student) students;
      } 
   
    }